#!/bin/bash
#www.oceandba.com
# 2021-12-07
# Version 1.0

# Working Directory
MAINDIR=

# Sourcing the configuration file
source $MAINDIR/conf/mariadb-data-duplicator.cnf

# Check Prerequisites
MYSQL=$(command -v mysql) || { echo >&2 "Command 'mysql' not found. Please install a MySQL/MariaDB client. Exit."; exit 1; }
MYSQLDUMP=$(command -v mysqldump) || { echo >&2 "Command 'mysqldump' not found. Please install a MySQL/MariaDB client. Exit."; exit 1; }
MYDUMPER=$(command -v mydumper) || { echo >&2 "Command 'mydumper' not found. Please run 'apt install mydumper' to install mydumper. Exit."; exit 1; }
MYLOADER=$(command -v myloader) || { echo >&2 "Command 'myloader' not found. Please run 'apt install mydumper' to install myloader. Exit."; exit 1; }
CHECK_SOURCE_DB=$($MYSQL -A --database=$SOURCE_DB --host=$SOURCE_HOST --user=$SOURCE_USER --password=$SOURCE_PASSWORD --port=$SOURCE_PORT -e 'select 0') || { echo >&2 "Could not connect to Source DB: $SOURCE_DB. Exit."; exit 1; }
CHECK_DESTINATION_DB=$($MYSQL -A --database=$DESTINATION_DB --host=$DESTINATION_HOST --user=$DESTINATION_USER --password=$DESTINATION_PASSWORD --port=$DESTINATION_PORT -e 'select 0') || { echo >&2 "Could not connect to Destination DB: $DESTINATION_DB. Exit."; exit 1; }
ZCAT=$(command -v zcat)
DATE_TODAY="$(date +%Y%m%d%H%M%S)" 
LOGFILE="$MAINDIR/logs/mydumper-$DATE_TODAY.log" 
# Repertoire de sauvegarde
TMPDUMP="$MAINDIR/mydumper" 

# Tables to ignore
IGNORE_TABLES=($(grep -o '^[^#]*' $MAINDIR/tables_a_ignorer.txt))

IGNORE_TABLES_CMD=()
for tbl in ${IGNORE_TABLES[@]};
  do IGNORE_TABLES_CMD+=("--ignore-table=$SOURCE_DB.$tbl")
done

# Create Dump Directory
[ -d "$TMPDUMP" ] || mkdir -p $TMPDUMP

if [ ! -z "$RETENTION_DUMP" ]; then
  if [ "$RETENTION_DUMP" -eq 0 ]; then
    find $TMPDUMP -name 'mysqldump_*.sql.gz' -exec rm {} \; >> $LOGFILE 2>&1
    find $TMPDUMP -type d -name 'dump_??????????????' -exec rm -rf {} \; >> $LOGFILE 2>&1
  fi
  # Drop the schema
  find $TMPDUMP -name 'mysqldump_*.sql.gz' -mtime +${RETENTION_DUMP} -exec rm {} \; >> $LOGFILE 2>&1
  # Remove previous dump files
  find $TMPDUMP -type d -name 'dump_??????????????' -mtime +${RETENTION_DUMP} -exec rm {} \; >> $LOGFILE 2>&1
fi

# Dump Schema
$MYSQLDUMP --single-transaction --no-data --triggers --routines --host=${SOURCE_HOST} --user=${SOURCE_USER} --password=${SOURCE_PASSWORD} --port=${SOURCE_PORT} ${SOURCE_DB} | gzip > ${TMPDUMP}/mysqldump_schema_${DATE_TODAY}.sql.gz

if [ $? -ne 0 ]; then
  echo "ERROR dumping schema" >> $LOGFILE 2>&1
  exit 1
fi

# Dump Directory
LATESTBACKUP="$TMPDUMP/dump_$DATE_TODAY" 
BACKUPARGS+=("--host=${SOURCE_HOST}" "--user=${SOURCE_USER}" "--password=${SOURCE_PASSWORD}" "--port=${SOURCE_PORT}" "--database=${SOURCE_DB}" "--outputdir=${LATESTBACKUP}" "--compress" "--no-schemas" "--logfile=${LOGFILE}" "--verbose=3" "--no-locks")

FINAL_EXPORT_ARGS="" 
# Ajuster les parametres comme definit dans le fichier de configuration
if [ ! -z "$ROWS" ]; then
   BACKUPARGS+=("--rows=${ROWS}")
   FINAL_EXPORT_ARGS="${BACKUPARGS[@]}" 
fi

if [ ! -z "$CHUNK_FILESIZE" ]; then
  BACKUPARGS+=("--chunk-filesize=${CHUNK_FILESIZE}")
  FINAL_EXPORT_ARGS="${BACKUPARGS[@]}" 
fi

if [ -z "$ROWS" ] || [ -z "$CHUNK_FILESIZE" ]; then
  FINAL_EXPORT_ARGS="${BACKUPARGS[@]}" 
fi

if [ ! -z "$EXPORT_THREADS" ]; then
   BACKUPARGS+=("--threads=${EXPORT_THREADS}")
   FINAL_EXPORT_ARGS="${BACKUPARGS[@]}" 
else
   FINAL_EXPORT_ARGS="${BACKUPARGS[@]}" 
fi

# Dump of data 
$MYDUMPER $FINAL_EXPORT_ARGS

if [ $? -ne 0 ]; then
  echo "ERROR dumping data" >> $LOGFILE 2>&1
  exit 1
fi

$ZCAT ${TMPDUMP}/mysqldump_schema_${DATE_TODAY}.sql.gz | mysql -A --database=${DESTINATION_DB} --host=${DESTINATION_HOST} --user=${DESTINATION_USER} --password=${DESTINATION_PASSWORD} --port=${DESTINATION_PORT} >> $LOGFILE 2>&1

if [ $? -ne 0 ]; then
  echo "ERROR importing schema" >> $LOGFILE 2>&1
  exit 1
fi

# Remove ignored table from dump
for tbl in "${IGNORE_TABLES[@]}";
do
  find $LATESTBACKUP -type f -name "${SOURCE_DB}.${tbl}.*.gz" -exec rm {} \; >> $LOGFILE 2>&1
done

# Adjust import parameters
IMPORTARGS+=("--host=${DESTINATION_HOST}" "--user=${DESTINATION_USER}" "--password=${DESTINATION_PASSWORD}" "--port=${DESTINATION_PORT}" "--database=${DESTINATION_DB}" "--directory=${LATESTBACKUP}" "--enable-binlog" "--verbose=3")
FINAL_IMPORT_ARGS="" 

if [ ! -z "$IMPORT_THREADS" ]; then
   IMPORTARGS+=("--threads=${IMPORT_THREADS}")
   FINAL_IMPORT_ARGS="${IMPORTARGS[@]}" 
else
   FINAL_IMPORT_ARGS="${IMPORTARGS[@]}" 
fi

# Load the data
$MYLOADER $FINAL_IMPORT_ARGS >> $LOGFILE 2>&1

if [ $? -ne 0 ]; then
  echo "$(date) - ERROR importing data" >> $LOGFILE 2>&1
  exit 1
else
  echo "$(date) - Export + Import successful" >> $LOGFILE 2>&1
  exit 0
fi
