# MySQL and MariaDB automatic data duplicator

This ansible role duplicates one or more schema's from one database server to another.

Pull requests are welcome, this role is actively maintained.

# Example usage:

Add this to roles/requirements.yml:
 - src: "git+https://gitlab.com/de-groot-consultancy-ansible-roles/mysql-mariadb-automatic-data-duplicator.git"                                                                                               
   name: mysql-mariadb-automatic-data-duplicator
   version: "1.0"

Then create your playbook:
** documentation WIP, pool requests are welcome **